﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Odp : MonoBehaviour
{
    [SerializeField] Text txt_odpowiedz;
    [SerializeField] GameObject odp_Empty;
    Tasks tasksScript;
    // Start is called before the first frame update
    void Start()
    {
        tasksScript = GetComponent<Tasks>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OdpowiedzWyswietl(int nrZad_p)
    {

        txt_odpowiedz.text = "";
        txt_odpowiedz.text = tasksScript.listaZadan3[nrZad_p].rozwiazanie;
        odp_Empty.SetActive(true);
        print("nr zadania: " + nrZad_p);
    }
    public void OdpowiedzZamknij()
    {
        odp_Empty.SetActive(false);
    }
}
