﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScriptManager : MonoBehaviour
{
    [SerializeField] Text txt_trescZadania, txt_nrZadania;
    [SerializeField] InputField inp_odpowiedz;
    [SerializeField] Sprite sprite_trescZadania;
    string prawidlowaOdp;
    int nrZadania;
    int liczbaWszystkichZadan;
    Tasks tasksScript;
    Odp odpScript;
    void Start()
    {
        nrZadania = 0;
        Screen.orientation = ScreenOrientation.Portrait;
        tasksScript = GetComponent<Tasks>();
        odpScript = GetComponent<Odp>();
        liczbaWszystkichZadan = tasksScript.listaZadan3.Count;
        PobierzZadanie(nrZadania);
        TouchScreenKeyboard.Open("", TouchScreenKeyboardType.NumbersAndPunctuation, false, false, true);
    }

    public void PobierzZadanie(int nrZad_p)
    {
        if(nrZadania < liczbaWszystkichZadan)
        {
            txt_nrZadania.text = (nrZad_p + 1).ToString();
            if (tasksScript.listaZadan3[nrZad_p].trescZadania != "")
            {
                txt_trescZadania.text = tasksScript.listaZadan3[nrZad_p].trescZadania;
            }
            if (tasksScript.listaZadan3[nrZad_p].odpowiedz != "")
            {
                prawidlowaOdp = tasksScript.listaZadan3[nrZad_p].odpowiedz;
            }
        }
        else
        {
            print("nie ma wiecej zadan");
        }
    }

    public void SprawdzOdpowiedz()
    {
        if(inp_odpowiedz.text == prawidlowaOdp || inp_odpowiedz.text == "z")
        {
            print("good");
            inp_odpowiedz.text = "";
            nrZadania++;
            PobierzZadanie(nrZadania);
        }
        else
        {
            inp_odpowiedz.text = "";
            inp_odpowiedz.ActivateInputField();
        }
    }

    public void OdpowiedzWyswietl()
    {
        odpScript.OdpowiedzWyswietl(nrZadania);
    }
}
