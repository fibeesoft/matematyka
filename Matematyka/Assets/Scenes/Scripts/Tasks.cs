﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Tasks : MonoBehaviour
{
    public string trescZadania { get;}
    public string odpowiedz { get;}
    public string trescZadaniaImg { get; }
    public string odpowiedzImg { get; }
    public string rozwiazanie { get; }
    public string rozwiazanieImg { get; }

    public List<Tasks> listaZadan3;

    public Tasks(string trescZadania_p, string odpowiedz_p, string rozwiazanie_p)
    {
        trescZadania = trescZadania_p;
        odpowiedz = odpowiedz_p;
        rozwiazanie = rozwiazanie_p;
    }


    public Tasks(string trescZadania_p, string odpowiedz_p, string trescZadaniaImg_p, string odpowiedzImg_p, string rozwiazanie_p, string rozwiazanieImg_p)
    {
        trescZadania = trescZadania_p;
        odpowiedz = odpowiedz_p;
        trescZadaniaImg = trescZadaniaImg_p;
        odpowiedzImg = odpowiedzImg_p;
        rozwiazanie = rozwiazanie_p;
        rozwiazanieImg = rozwiazanieImg_p;
    }


    private void Awake()
    {
        listaZadan3 = new List<Tasks>();

        listaZadan3.Add(new Tasks("Różnica dwóch liczb wynosi 457. Mniejsza z tych liczb jest równa 128.\nIle wynosi większa liczba ? ", "585", "Różnica to wynik odejmowania.\nZ treści zadania wiemy, że a - b = 457. \n Mniejsza liczba wynosi 128, a zatem: a - 128 = 457. \n a = 457 + 128 = 585"));
        listaZadan3.Add(new Tasks("Klasa I zebrała 61 kg makulatury, klasa II o 29 kg więcej niż klasa I, zaś klasa III o 23 kg mniej niż klasa II. \nIle kilogramów makulatury zebrały te trzy klasy ? ", "218", "Klasa I - 61kg \nKlasa II - 61kg + 29kg = 90kg \nKlasa III - 90kg - 23kg = 67kg \n 61kg + 90kg + 67kg = 218kg"));
        listaZadan3.Add(new Tasks("Na kolonie pojechało 131 osób trzema autokarami. W pierwszym i drugim autokarze było 86 dzieci, a w drugim i trzecim autobusie 87 dzieci. \n\nIle dzieci było w drugim autobusie?", "42", "We wszystkich autobusach było 131 dzieci, zatem: I + II + III = 131 \n W pierwszym i drugim autobusie było 86 dzieci, zatem: I + II = 86, dzięki temu obliczymy ilość dzieci w III autobusie: 131 - 86 = 45. \n\rWiemy z treści, że w II i III autobusie było 87 dzieci. Ponieważ w III autobusie jest 45 dzieci zatem w drugim jest 87 - 45 = 42 dzieci."));
        listaZadan3.Add(new Tasks("Suma trzech liczb wynosi 941. Pierwsza liczba to 184, druga jest o 68 większa od pierwszej.\nZnajdź trzeci składnik tej sumy.", "505", "Wiemy, że a + b + c = 941\na = 184 \nb = 184 + 68 = 252\nPodstawiamy wartości do równania: 184 + 252 + c = 941 \n c = 941 - 184 - 252 = 505"));
        listaZadan3.Add(new Tasks("Arek zapisał się w lutym na miesięczny kurs programowania. Zajęcia odbywają się w każdy wtorek i czwartek od 15.00 do 18.30.\nIle godzin trwa cały kurs?", "28", "Miesiąc liczy 4 tygodnie, zatem łączna ilość dni zajęć to 2 x 4 = 8.\nKurs trwa 3,5 godziny (18.30 - 15.00 = 3,5)\nŁączna ilość godzin to:\n8 x 3,5 = 28 godzin."));
        listaZadan3.Add(new Tasks("Marta ma tyle złotówek, ile jest dni w drugim kwartale roku. Kupiła książkę za 15 złotych i 2 soki po 5,50zł każdy. Ile pieniędzy brakuje jej do 100 zł po tych zakupach?", "35", "Ilość dni w drugim kwartale: 30+31+30=91\nMarta ma 91zł.\nZakupy Marty kosztują: 15zł + 5,50zł + 5,50zł = 26zł\nMarcie zostaje 91zł-26zł = 65zł.\nDo 100zł brakuje jej 35zł."));
        listaZadan3.Add(new Tasks("Ani szkoła znajduje się w odległości 7km od jej domu. Ile kilometrów do szkoły i ze szkoły przejechała Ania w tym tygodniu, jeśli w piątek zajęcia były odwołane?","56", "Ania przejeżdza codziennie 7km x 2 = 14km.\nW tym tygodniu uczyła się przez 4 dni, zatem\n14km x 4 = 56km"));
        listaZadan3.Add(new Tasks("Pies, kot i Krzyś ważą razem 19kg. Pies waży o 1kg więcej niż kot, a Krzyś jest 4 razy cięższy od kota. Ile waży Krzyś?", "12", "pies + kot + Krzyś = 19kg\n pies = kot + 1kg\nKrzyś = kot x 4\nTeraz należy podstawić dan do pierwszego równania:\nkot + 1kg + kot + kot x 4 = 19kg\nkot x 6 + 1kg = 19kg\nkot x 6 = 19kg - 1kg\nkot x 6 = 18kg\nJeden kot waży 18kg/6 = 3kg.\nKrzyś waży tyle co 4 koty,zatem 3kg x 4 = 12kg."));
        listaZadan3.Add(new Tasks("","",""));
    }

}
